package com.mitocode.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.mitocode.app.Documento;

public class DocumentoUnit {

	@Test
	public void buscarPersonaCP1() {	
		assertTrue(Documento.buscarPersona("DNI", "72301305").equalsIgnoreCase("OK"));
	}
	
	@Test
	public void buscarPersonaCP2() {	
		assertTrue(Documento.buscarPersona("RUC", "0").equalsIgnoreCase("ERROR"));
	}
}
